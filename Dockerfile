FROM nginxinc/nginx-unprivileged:1-alpine

LABEL maintainer="dhiabi.akram@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY uwsgi_params /etc/nginx/uwsgi_params

# default env variables values
ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# switch to user root in order to create new directories and files and change permissions
USER root

RUN mkdir -p /vol/static
RUN chmod 755  /vol/static

# create an empty default.conf file for nginx to populate the template and env variables
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh 
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"] 
