# recipe-app-api-proxy

nginx proxy app for our recipe api

## Usage


## Env variables

* `LISTEN_PORT`= Nginx Port to listen to (default `8000`)
* `APP_HOST` = Hostname app to forward request to (default `app`)
* `APP_PORT`=  Port of app to forward requests to (default `9000`)