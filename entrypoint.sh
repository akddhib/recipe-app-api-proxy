#! /bin/sh

# "set -e" stops the execution of a script if a command or pipeline has an error and return the error to the screen
set -e

# "envsubst" program substitutes the values of environment variables.
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# tell nginx to run in foreground of docker execution -- disable background 
# this way all nginx logs will be printed to docker output
nginx -g 'daemon off;'